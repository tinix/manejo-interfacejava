package datos;

/**
 *
 * @author tinix
 */
public interface AccesoDatos {
    
    public static int MAX_REGISTROS = 10; // es un const public y static
    
    public abstract void insertar();
    
    public abstract void listar();
    
}
